import React, { Component } from 'react'
import ComponentView from './view'
import { message } from 'antd'

/**
 * @description Sample Component
 * @type Component
 * @author Jasjot
 */
export default class Main extends Component {

    /**
     * Container
     * @param props
     */
    constructor (props) {
        super(props)
        this.state = {
            loading: false,
        }
        this.beforeUpload = this.beforeUpload.bind(this)
        this.handleChange = this.handleChange.bind(this)
    }

    getBase64 (img, callback) {
        const reader = new FileReader()
        reader.addEventListener('load', () => callback(reader.result))
        reader.readAsDataURL(img)
    }

    beforeUpload (file) {
        const isJPG = file.type === 'image/jpeg'
        if (!isJPG) {
            message.error('You can only upload JPG file!')
        }
        const isLt2M = file.size / 1024 / 1024 < 2
        if (!isLt2M) {
            message.error('Image must smaller than 2MB!')
        }
        return isJPG && isLt2M
    }

    handleChange (info) {
        if (info.file.status === 'uploading') {
            this.setState({loading: true})
            return
        }
        if (info.file.status === 'done') {
            // Get this url from response in real world.
            this.getBase64(info.file.originFileObj, imageUrl => this.setState({
                imageUrl,
                loading: false,
            }))
        }
    }

    /**
     * ComponentDidMount Hook
     */
    componentDidMount () {

    }

    /**
     * Render Method
     * @returns {*}
     */
    render () {
        return (ComponentView.bind(this))()
    }
}

Main.displayName = 'Image-Picker-Component'
