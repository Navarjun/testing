import React from 'react'
import { Upload, Icon } from 'antd'
import './style.css'

var view = function () {
    const {loading, imageUrl} = this.state
    const uploadButton = (
        <div>
            <Icon type={loading ? 'loading' : 'plus'}/>
            <div className="ant-upload-text">Upload</div>
        </div>
    )

    return (
        <Upload
            name="avatar"
            listType="picture-card"
            className="avatar-uploader"
            showUploadList={false}
            action="http://jsonplaceholder.typicode.com/posts/"
            beforeUpload={this.beforeUpload}
            onChange={this.handleChange}
        >
            {imageUrl ? <img src={imageUrl} alt="avatar"/> : uploadButton}
        </Upload>
    )
}

export default view
