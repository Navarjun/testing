import { Component } from 'react'
import ComponentView from './view'
import data from './config'

/**
 * @description Sample Component
 * @type Component
 * @author Jasjot
 */
export default class Main extends Component {

    /**
     * Container
     * @param props
     */
    constructor (props) {
        super(props)
        this.state = {
            data: data
        }
    }

    onChange (a, b, c) {
        //console.log(a, b, c)
    }

    /**
     * Shuffle posts
     */
    shuffleArray(array) {
        let i = array.length - 1;
        for (; i > 0; i--) {
            const j = Math.floor(Math.random() * (i + 1));
            const temp = array[i];
            array[i] = array[j];
            array[j] = temp;
        }
        return array;
    }

    /**
     * ComponentDidMount Hook
     */
    componentDidMount () {

    }

    /**
     * Render Method
     * @returns {*}
     */
    render () {
        return (ComponentView.bind(this))()
    }
}

Main.displayName = 'App-Component'
