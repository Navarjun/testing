import React from 'react'
import { Carousel, Icon } from 'antd'
import './style.css'

let view = function () {
    const {data} = this.state
    const shuffledImages = this.shuffleArray(data)

    return (
        <div className="animated fadeIn home">
            <div className="content">
                <Carousel
                    afterChange={this.onChange.bind(this)}
                    dots={false}
                    arrows={true}
                    effect={'fade'}
                    nextArrow={<Icon type="right-circle-o"/>}
                    prevArrow={<Icon type="left-circle-o"/>}
                    lazyLoad={true}
                >
                    {
                        shuffledImages.map((item, index) => {
                            return (
                                <div className="slide" key={index}>
                                    <div className="image" style={{backgroundImage: 'url(' + item.image + ')'}}/>
                                    <div className="author">
                                        <a href={item.link} target="_blank">
                                            <span>Picture by {item.author}</span>
                                            {
                                                (item.website) ? (
                                                    <span> / {item.website}</span>
                                                ) : null
                                            }
                                        </a>
                                    </div>
                                </div>
                            )
                        })
                    }
                </Carousel>
            </div>
        </div>
    )
}

export default view
