import bg1 from '../../images/1.jpg'
import bg2 from '../../images/2.jpg'
import bg3 from '../../images/3.jpg'
import bg4 from '../../images/4.jpg'
import bg5 from '../../images/5.jpg'
import bg6 from '../../images/6.jpg'
import bg7 from '../../images/7.jpg'
import bg8 from '../../images/8.jpg'
import bg9 from '../../images/9.jpg'
import bg10 from '../../images/10.jpg'
import bg11 from '../../images/11.jpg'
import bg12 from '../../images/12.jpg'
import bg13 from '../../images/13.jpg'
import bg14 from '../../images/14.jpg'
import bg15 from '../../images/15.jpg'
import bg16 from '../../images/16.jpg'
import bg17 from '../../images/17.jpg'
import bg18 from '../../images/18.jpg'
import bg19 from '../../images/19.jpg'
import bg20 from '../../images/20.jpg'
import bg21 from '../../images/21.jpg'
import bg22 from '../../images/22.jpg'


const data = [
    {
        image: bg1,
        author: '12019',
        website: 'Pixabay',
        link: 'https://pixabay.com/photos/scotland-landscape-mountains-hills-1761292/'
    },
    {
        image: bg2,
        author: 'Jplenio',
        website: 'Pixabay',
        link: 'https://pixabay.com/photos/nature-waters-lake-island-3082832/'
    },
    {
        image: bg3,
        author: 'Jpeter2',
        website: 'Pixabay',
        link: 'https://pixabay.com/photos/landscape-autumn-twilight-mountains-615428/'
    },
    {
        image: bg4,
        author: 'Danfador',
        website: 'Pixabay',
        link: 'https://pixabay.com/photos/dawn-sun-mountain-landscape-sky-190055/'
    },
    {
        image: bg5,
        author: 'MarjonBesteman',
        website: 'Pixabay',
        link: 'https://pixabay.com/photos/bird-lorikeet-parrot-birds-animals-4612175/'
    },
    {
        image: bg6,
        author: 'OlcayErtem',
        website: 'Pixabay',
        link: 'https://pixabay.com/photos/bridal-son-in-law-wedding-double-4615557/'
    },
    {
        image: bg7,
        author: 'guvo59',
        website: 'Pixabay',
        link: 'https://pixabay.com/photos/cat-small-kitten-domestic-cat-pet-4611189/'
    },
    {
        image: bg8,
        author: 'zoegammon',
        website: 'Pixabay',
        link: 'https://pixabay.com/photos/dog-puppy-yorkie-animal-cute-4608266/'
    },
    {
        image: bg9,
        author: 'Alexas_Fotos',
        website: 'Pixabay',
        link: 'https://pixabay.com/photos/flamingo-bird-colorful-feather-4553025/'
    },
    {
        image: bg10,
        author: 'Alexas_Fotos',
        website: 'Pixabay',
        link: 'https://pixabay.com/photos/koi-pond-fish-japanese-nature-4616816/'
    },
    {
        image: bg11,
        author: 'ottawagraphics',
        website: 'Pixabay',
        link: 'https://pixabay.com/photos/la-skatepark-skating-skateboarding-4618922/'
    },
    {
        image: bg12,
        author: 'enriquelopezgarre',
        website: 'Pixabay',
        link: 'https://pixabay.com/photos/landscape-cave-rock-sunset-sky-4518195/'
    },
    {
        image: bg13,
        author: 'enriquelopezgarre',
        website: 'Pixabay',
        link: 'https://pixabay.com/photos/landscape-path-elephants-light-4566020/'
    },
    {
        image: bg14,
        author: 'Heidelbergerin',
        website: 'Pixabay',
        link: 'https://pixabay.com/photos/landscape-sky-clouds-mountains-4593909/'
    },
    {
        image: bg15,
        author: 'blank76',
        website: 'Pixabay',
        link: 'https://pixabay.com/photos/matera-cityscape-italy-city-sassi-4612016/'
    },
    {
        image: bg16,
        author: 'ogecnol',
        website: 'Pixabay',
        link: 'https://pixabay.com/photos/morning-boat-pier-water-sunrise-4573991/'
    },
    {
        image: bg17,
        author: 'suju',
        website: 'Pixabay',
        link: 'https://pixabay.com/photos/owl-bird-eagle-owl-close-up-bill-4590569/'
    },
    {
        image: bg18,
        author: 'Alpcem',
        website: 'Pixabay',
        link: 'https://pixabay.com/photos/portrait-girl-woman-face-female-4597853/'
    },
    {
        image: bg19,
        author: 'radu_floryn22',
        website: 'Pixabay',
        link: 'https://pixabay.com/photos/portrait-autumn-red-woman-female-4599559/'
    },
    {
        image: bg20,
        author: 'Alpcem',
        website: 'Pixabay',
        link: 'https://pixabay.com/photos/portrait-girl-woman-beauty-4618467/'
    },
    {
        image: bg21,
        author: 'eluxirphoto',
        website: 'Pixabay',
        link: 'https://pixabay.com/photos/squirrel-nature-cute-wild-mammals-4515962/'
    },
    {
        image: bg22,
        author: 'Capri23auto',
        website: 'Pixabay',
        link: 'https://pixabay.com/photos/white-ling-butterfly-butterflies-4613574/'
    },

]

export default data