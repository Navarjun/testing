import React from 'react'
import './style.css'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import Header from '../header'
import Home from '../home'

let view = function () {
    return (
        <Router>
            <div className="app">
                <Header/>
                <Switch>
                    <Route path={'/'} exact component={Home}/>
                    <Route path={'/home'} component={Home}/>
                </Switch>
            </div>
        </Router>
    )
}

export default view
