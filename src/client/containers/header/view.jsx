import React from 'react'
import { Layout } from 'antd'
import logo from '../../images/logo.png'
import './style.css'

const {Header} = Layout

let view = function () {
    return (
        <div className="header">
            <Header>
                 <div className="logo">
                    <img src={logo} alt="JGI"/>
                </div>
            </Header>
        </div>
    )
}

export default view
